'''
This example is a workaround that help you to use the camera in your smartphone and stream video.
You must be worry when you do not have an integrated camera (Raspberry camera for exemple) or a usb
webcam when your work with jetson nano.

Before running this example, do the following things:
  (*) Install IP Webcam for android phone (https://play.google.com/store/apps/details?id=com.pas.webcam)
      NB: For IOS phone, search the equivalent

  (*) launch the app on your mobile and start the server

  (*) keep safely the stream ip adress (http://ipOfPhone:8080). We will us it in the code 

  (*) The phone and the device who want to acceed to the camera (jetson, PC, raspberry,...) should be in the same network 

'''

import urllib.request
import cv2
import numpy as np
import time

#Replace URL content with yourth
URL = "http://192.168.1.34:8080/video"
vid = cv2.VideoCapture(URL) 
while True:
    ret, img = vid.read() 
    cv2.imshow('Live Streaming via Mobile',img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
